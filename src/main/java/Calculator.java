public class Calculator {
    public int sum(int a, int b) {
        return a + b;
    }

    public int subtract(int a, int b) {
        return a - b;
    }

    public double division(double a, double b) {
        return a / b;
    }

    public int product(int a, int b) {
        return a * b;
    }
}
