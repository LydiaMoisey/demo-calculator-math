import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    //Sum
    @Test
    public void testSum1() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(2, 2);

        Assert.assertEquals(4, actual);
    }

    @Test
    public void testSum2() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(Integer.MAX_VALUE, 0);

        Assert.assertEquals(Integer.MAX_VALUE, actual);
    }

    @Test
    public void testSum3() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(Integer.MIN_VALUE, 0);

        Assert.assertEquals(Integer.MIN_VALUE, actual);
    }

    @Test
    public void testSum4() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(Integer.MAX_VALUE, 1);

        Assert.assertEquals(2147483647, actual);
    }

    @Test
    public void testSum5() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(Integer.MIN_VALUE, -1);

        Assert.assertEquals(-2147483648, actual);
    }

    // Subtract
    @Test
    public void testSubtract1() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(5, 8);

        Assert.assertEquals(-3, actual);
    }

    @Test
    public void testSubtract2() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(Integer.MAX_VALUE, 0);

        Assert.assertEquals(Integer.MAX_VALUE, actual);
    }

    @Test
    public void testSubtract3() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(Integer.MIN_VALUE, 0);

        Assert.assertEquals(Integer.MIN_VALUE, actual);
    }

    @Test
    public void testSubtract4() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(Integer.MAX_VALUE, -2);

        Assert.assertEquals(2147483647, actual);
    }

    @Test
    public void testSubtract5() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(Integer.MIN_VALUE, -2);

        Assert.assertEquals(-2147483648, actual);
    }

    // Division
    @Test
    public void testDivision1() {
        Calculator calculator = new Calculator();
        double actual = calculator.division(10, 3.2);

        Assert.assertEquals(3.125, actual,0.00003);
    }

    @Test
    public void testDivision2() {
        Calculator calculator = new Calculator();
        double actual = calculator.division(Double.MAX_VALUE, 1);

        Assert.assertEquals(Double.MAX_VALUE, actual,0.00003);
    }

    @Test
    public void testDivision3() {
        Calculator calculator = new Calculator();
        double actual = calculator.division(Double.MIN_VALUE, 1);

        Assert.assertEquals(Double.MIN_VALUE, actual, 0.00003);
    }

    @Test
    public void testDivision4() {
        Calculator calculator = new Calculator();
        double actual = calculator.division(5, 0);

        Assert.assertEquals(0, actual, 0.00003);
    }

    @Test
    public void testDivision5() {
        Calculator calculator = new Calculator();
        double actual = calculator.division(Double.MAX_VALUE, 0.5);

        Assert.assertEquals(1.7976931348623157e+308, actual, 0.00003);
    }
    @Test
    public void testDivision6() {
        Calculator calculator = new Calculator();
        double actual = calculator.division(Double.MIN_VALUE, 0.5);

        Assert.assertEquals(-1.7976931348623157e+308, actual, 0.00003);
    }

// Product
@Test
public void testProduct1() {
    Calculator calculator = new Calculator();
    int actual = calculator.product(-4, 2);

    Assert.assertEquals(-8, actual);
}

    @Test
    public void testProduct2() {
        Calculator calculator = new Calculator();
        int actual = calculator.product(Integer.MAX_VALUE, 1);

        Assert.assertEquals(Integer.MAX_VALUE, actual);
    }

    @Test
    public void testProduct3() {
        Calculator calculator = new Calculator();
        int actual = calculator.product(Integer.MIN_VALUE, 1);

        Assert.assertEquals(Integer.MIN_VALUE, actual);
    }

    @Test
    public void testProduct4() {
        Calculator calculator = new Calculator();
        int actual = calculator.product(Integer.MAX_VALUE, 2);

        Assert.assertEquals(2147483647, actual);
    }

    @Test
    public void testProduct5() {
        Calculator calculator = new Calculator();
        int actual = calculator.product(Integer.MIN_VALUE, 2);

        Assert.assertEquals(-2147483648, actual);
    }
}